#ifndef HTTP_LINUXAPI_HPP
#define HTTP_LINUXAPI_HPP

#include <vector>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <netdb.h> 

#include <arpa/inet.h>

using namespace std;

namespace http {

    class LINUXAPI {
    private:

    public:
        vector<std::string> allowed_methods;
        vector<string> server_list;			 
        
        string resource; //Resource to fetch. Eg. /index.html, or /listOfCars
        string res;
        
        bool cache_data; //True or false. Default false.
        bool cache_exists;
       
        int port;

        /*------------ Functions -----------*/
        LINUXAPI();
        LINUXAPI(string resource, bool cache_data, string server);
        
        string getResponse(string body);
        string getBody(string http);
        
        string get_server();
        void add_server(string server);
        
        string getRequestHeader();
        void connect_to_API(int client);
        
        void error(const char *msg);
    };

    /*------------------------------------------------------------------------------*/

    LINUXAPI::LINUXAPI() {}
    LINUXAPI::LINUXAPI(string resource, bool cache_data, string server_ip) {
        this->cache_data = cache_data;
        this->cache_exists = false;
        this->allowed_methods.push_back("GET");
        this->resource = resource;
        this->port = 80;
        this->add_server(server_ip);
    }

    void LINUXAPI::connect_to_API(int client) {
        int sockfd, n;
        struct sockaddr_in serv_addr;
        string buffer;

        if (this->cache_exists == true && this->cache_data == true) {
            printf("Cached data");
			//Do nothing. The cached response will be written to client after if/else
        }
		
        else {
            sockfd = socket(AF_INET, SOCK_STREAM, 0);
            if (sockfd < 0) {
                error("ERROR opening socket");
            }
            
            in_addr_t data;
            data = inet_addr(this->get_server().c_str());
            struct hostent *server = gethostbyaddr(&data, 4, AF_INET);
        
            if (server == NULL) {
                fprintf(stderr, "ERROR, no such host\n");
                exit(0);
            }

            serv_addr.sin_family = AF_INET;
            bcopy((char *) server->h_addr,
                    (char *) &serv_addr.sin_addr.s_addr,
                    server->h_length);

            serv_addr.sin_port = htons(this->port);
            if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0)
                error("ERROR connecting");

            buffer = getRequestHeader();

            n = write(sockfd, buffer.c_str(), buffer.length());
            if (n < 0) { error("ERROR writing to socket"); }

            char buff[4096];
            //Les Content-Length, og les bytes accordingly.
            n = read(sockfd, buff, 4095);
            if (n < 0) {
                error("ERROR reading from socket");
            };
            close(sockfd);
            if(cache_data == true) {
               cache_exists = true;
            }

            string httpBody = getBody(buff);
            this->res = "";
            this->res = getResponse(httpBody);
        }
        
        n = write(client, res.c_str(), res.length());
        if (n < 0) printf("ERROR writing to socket\n");
        close(client);
    }

	void LINUXAPI::add_server(string server_ip) {
        this->server_list.push_back(server_ip);
    }
    
    string LINUXAPI::get_server() {
        return this->server_list.at(rand() % this->server_list.size());
    }

    string LINUXAPI::getRequestHeader() {
        return "GET /index.html HTTP/1.1 \n" \
                                      "Host: Ubuntu \n" \
                                      "Connection: close\r\n\r\n";
    }

    string LINUXAPI::getResponse(string httpBody) {
        string header = "HTTP/1.1 200 OK \n" \
               "Content-Type: text/plain \n";
        header.append("Content-Length: " + to_string(httpBody.size() - 4) + " \n");
        header.append("Connection: close");
        header.append(httpBody);
        return header;
    }

    string LINUXAPI::getBody(string http) {
        std::size_t pos = http.find("\r\n\r\n");
        std::string body = http.substr(pos);
        return body;
    }
	
	void LINUXAPI::error(const char *msg) {
        perror(msg);
        exit(1);
    }

}
#endif //API_HPP
