#ifndef HTTP_API_HPP
#define HTTP_API_HPP

#include <vector>
#include <response.hpp>
#include <request.hpp>
#include <message.hpp>
#include <algorithm>
#include <mime_types.hpp>


using namespace std;
using Connection_ptr = shared_ptr<net::TCP::Connection>;

#define content_type http::header_fields::Entity::Content_Type
#define html         extension_to_type("html")

namespace http {
    class API {
        private:

    public:
        vector<string> allowed_methods; //POST - GET - PUT - DELETE        
        vector<net::TCP::Socket> server_list;//IP + Port. Enables loadbalancing. DNS will not be prioritised for this thesis, but is a feature creep.        

        string resource;          //Resource to fetch. Eg. /index.html, or /listOfCars
        string res;
        
        bool   cache_data;        //True or false. Default false.
        bool   cache_exists;
                        
        /*------------ Functions -----------*/
        API();
        API(string resource, net::TCP::Socket server, bool cache_data);
        
		string getResponse(string httpBody);
		string getBody(string http);
		
		net::TCP::Socket get_server();
        void add_server(net::TCP::Socket server);
        
        string getRequestHeader();
        void connect_to_API(shared_ptr<net::Inet4<VirtioNet> > inet, Connection_ptr client);
    };
/*------------------------------------------------------------------------------*/ 

    API::API(){};
    API::API(string resource, net::TCP::Socket server, bool cache_data) { 
        this->cache_data = cache_data;
        this->allowed_methods.push_back("GET");
        this->cache_exists = false;
        this->resource = resource;
        this->add_server(server);
    }
    
    void API::connect_to_API(shared_ptr<net::Inet4<VirtioNet> > inet, Connection_ptr client) {                
        if(cache_data == true && cache_exists == true) {
           client->write(this->res.data(), this->res.size(), [](size_t n) {
				});
			client->close();
            return;
        }
        
          
 		auto outgoing = inet->tcp().connect(this->get_server());
		outgoing->onConnect([this, client](shared_ptr<net::TCP::Connection> API_server) {
				string httpReq = this->getRequestHeader();
			
                API_server->write(httpReq.data(), httpReq.size(), [](size_t n) {
					   //Do nothing
					  });
			
			API_server->read(255, [this, client](net::TCP::buffer_t buf, size_t n) {
				string data { (char*)buf.get(), n };
				string httpBody = this->getBody(data);
				this->res = "";
				this->res = getResponse(httpBody);
				cache_exists = true;
				client->write(this->res.data(), this->res.size(), [](size_t n) {
				   	//Do nothing
				});
			
				client->close();
			});
			API_server->close();
		});
    }
    
    void API::add_server(net::TCP::Socket server) {
        this->server_list.push_back(server);
    }
    
    net::TCP::Socket API::get_server() {
        return this->server_list.at(rand() % this->server_list.size());
    }
    
    string API::getRequestHeader() {
    return "GET " + this->resource + " HTTP/1.1 \n" \
                                      "Host: IncludeOS \n" \
                                      "Connection: close\r\n\r\n";
    }
	string API::getResponse(string httpBody) {
        string header = "HTTP/1.1 200 OK \n" \
               "Content-Type: text/plain \n";
        header.append("Content-Length: " + to_string(httpBody.size() - 4) + " \n");
        header.append("Connection: close");
        header.append(httpBody);
        return header;
    }

    string API::getBody(string http) {
        std::size_t pos = http.find("\r\n\r\n");
        std::string body = http.substr(pos);
        return body;
    }
    
}
#endif //API_HPP
