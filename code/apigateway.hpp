#ifndef HTTP_APIGATEWAY_HPP
#define HTTP_APIGATEWAY_HPP
#include <api.hpp>
#include <response.hpp>
#include <message.hpp>
#include <mime_types.hpp>
#include <request.hpp>

using namespace std;
using Connection_ptr = shared_ptr<net::TCP::Connection>;
shared_ptr<net::Inet4<VirtioNet> > inet;

string getFunction(string request) {
    string function;
    for(int i = 4;;i++) {
      if (isspace(request.at(i))) {
        break;
      }
      function += request.at(i);
    }
    return function;
  }

namespace http {
class Apigateway {
    private:
public: 
	map<string, API> API_list; //Contains a list of <function> --> <API> mapping. Eg. /cars -> carAPI
    Apigateway();
    void init_API_list();
};

Apigateway::Apigateway() {
	//net::TCP::set_MSL(5s);
    hw::Nic<VirtioNet>& eth0 = hw::Dev::eth<0, VirtioNet>();
    inet = make_shared<net::Inet4<VirtioNet> >(eth0); 
    
	inet->network_config({10,1,0,42},    // IP
			{ 255,255,255,0 },  // Netmask
			{ 10,1,0,1 },       // Gateway
			{ 8,8,8,8 } );      // DNS
    auto& server = inet->tcp().bind(80);
        
    init_API_list();

	
	server.onAccept([](auto conn) -> bool {
      return true; // allow all connections
    
	}).onConnect([this](auto conn) {
        conn->read(255, [conn, this](net::TCP::buffer_t buf, size_t n) {
			string data { (char*)buf.get(), n };            
            string function = getFunction(data.c_str());
            
			if(!(this->API_list.find(function) == this->API_list.end())) {
                this->API_list[function].connect_to_API(inet, conn);
            }
          });
      });
}

void Apigateway::init_API_list() {
    net::TCP::Socket server_api1 = net::TCP::Socket{{10,1,0,1}, 80};
    API_list.insert(map<string, API>::value_type("/api1", API("/index.html", server_api1, true)));
}
	
}
#endif //< APIGATEWAY_HPP

