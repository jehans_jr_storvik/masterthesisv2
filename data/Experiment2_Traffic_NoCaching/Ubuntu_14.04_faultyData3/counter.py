# -*- coding: utf-8 -*-
import os, os.path

#All files, INCREMENT 1-N for a given experiment.
lowestKernel = 0
lowestUser = 0
lowestCPU = 0
lowestFile = ""

highestKernel = 0
highestUser = 0
highestCPU = 0
highestFile = ""

avgCPU = 0
avgCpuAll12 = 0
totCon = 0
totRep = 0

fileList = []

files_in_dir = os.listdir("./")
for file in files_in_dir:
	if(file == "counter.py"):
		print "counter.py"
	else:
		perc = 0
		avgCPU = 0
		data = open(file, "r")
		counter = 0
		for line in data:
			line = line.strip().split(",")
			#perc += float(line[0])
			counter += 1

			if("," in line[0]):
				line[0] = line[0].strip(",")
				
			perc += float(line[0])
			if(counter == 300):
				user = float(line[1].strip(" "))
				kernel = float(line[3].strip(" "))

				totalCPU = user + kernel 

				avgCPU += totalCPU
				
		print "-------------------------"
		print "Looping line - ", file
		print "AvgCPU = ", avgCPU/12
		print "TotalCPU", totalCPU
		print "Cpu Percentage this run",  (perc/300)
		avgCpuAll12 += (perc/300)
		
		print "-------------------------"
				#print [5] #Memory. Expected to be equal
	print "avgCpuAll12Runs", avgCpuAll12/12


