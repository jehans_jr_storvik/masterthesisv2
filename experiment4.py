   # -*- coding: utf-8 -*-
#This is a version built upon the dataGathere.copy.3.Mai.
#Its purpose is to create a test 
from httperfpy import Httperf
import subprocess
import os
import time
import psutil

from subprocess import check_output

OS="IncludeOS"
amountOfRuns = 300			# 1 = 1 second
INCREMENTS=[10,50,100,200,500,1000] #,20,50,100
#
for RATE in INCREMENTS:
	perf = Httperf(
		server="10.1.0.42",
		uri="/api1",
		port=80,
		timeout=5,
		num_conns=RATE,
		rate = RATE,
		)
	perf.parser = True
	failedRuns = 0
	x = 1
	while(x <= 12):
		child = subprocess.Popen(['pgrep qemu'], stdout=subprocess.PIPE, shell=True)
		pid = int(child.communicate()[0].strip())
		print "PID = ", str(pid)
		#OS_seconds_rate_totalCon_x
		filename = "./data/Experiment4_10secTraffic_Caching/"+ OS + "/" + str(RATE)  + "_" + str(x)
		print "Starting to write to " , filename

		target = open(filename, 'w')
		startTime = time.time()
		
		counter = 0
		startUser = 0
		lastUser = 0
		startKernel = 0
		lastKernel = 0
		timeSpent = 0
		process = psutil.Process(pid)

		while counter < amountOfRuns:
			endTime = time.time()
			timeSpent = endTime - startTime

			cpuPercentage = process.cpu_percent()
			cpuCycles = process.cpu_times()
			if(counter == 0):
				lastUser = startUser = cpuCycles[0]
				lastKernel = startKernel = cpuCycles[1]

			tts = 1
			percent = 100
			if(counter % 10 == 0): #5 minutes
				results = perf.run()
				percent = (int(results["total_replies"]) / int(results["total_connections"])) * 100	
				line = str((cpuPercentage , (cpuCycles[0] - startUser) , (cpuCycles[0] - lastUser) , (cpuCycles[1] - startKernel), (cpuCycles[1] - lastKernel), process.memory_percent(), timeSpent, results["total_connections"] , results["total_requests"], results["total_replies"], results["request_rate_per_sec"], "req/s", percent , "%", results["total_test_duration"]))
				tts = 1 - float(results["total_test_duration"])
			else:
				line = str((cpuPercentage , (cpuCycles[0] - startUser) , (cpuCycles[0] - lastUser) , (cpuCycles[1] - startKernel), (cpuCycles[1] - lastKernel), process.memory_percent(), timeSpent, 0 , 0, 0, 0, "req/s", 0 , "%", 0))
			
			lastUser = cpuCycles[0]
			lastKernel = cpuCycles[1]
			
			line = line.strip("(")
			line = line.strip(")")
			target.write(line + "\n")
			print line
			counter += 1
			if(tts > 0):
				time.sleep(tts)
			
			if(percent < 40):
				failedRuns += 1
				print "Starting experiment over, Number = ", failedRuns
				process.terminate() 
				subprocess.Popen(['/home/jst/IncludeOS/examples/tcp/run.sh ./IncludeOS_Demo_Service.img'], stdout=subprocess.PIPE, shell=True)
				time.sleep(3)
				x -= 1
				break
		x+= 1
		target.close()
