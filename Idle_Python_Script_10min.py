   # -*- coding: utf-8 -*-
from httperfpy import Httperf
import subprocess
import time
import psutil

from subprocess import check_output

child = subprocess.Popen(['pgrep qemu'], stdout=subprocess.PIPE, shell=True)
pid = int(child.communicate()[0].strip())

print "PID = ", str(pid)
RATE = 100
NUMCOM = 500

perf = Httperf(
	server="192.168.179.50",
	port=80,
	num_conns=RATE,
	rate = RATE,
	)
perf.parser = True
command = "/proc/" + str(pid) + "/stat"
print "Command =", command
print "---------------------------------------------------------------------------"
#print "<Total connections> <Total requests> <Total replies> <Request rate/second> <Percent reply rate> <Total seconds in> <Nr>" 
#print "CPU% U-cyclesTotal U-cycles1sec K-cyclesTotal K-cycles1sec Memory% timeElapsed"

for x in range(1,10):
	filename = "./data/IncludeOS_Idle_10min-" + str(x)
	print "Starting to write to " , filename
	target = open(filename, 'a')
	startTime = time.time()
	timeToMonitor = 10 #Expressed in seconds.
	currentCPUcycle = 0
	total = 0
	original = 0
	counter = 0

	startUser = 0
	lastUser = 0
	startKernel = 0
	lastKernel = 0

	timeSpent = 0


	while timeSpent < (600):
		process = psutil.Process(pid)
		endTime = time.time()

		cpuPercentage = process.cpu_percent(1)
		cpuCycles = process.cpu_times()
		if(counter == 0):
			lastUser = startUser = cpuCycles[0]
			lastKernel = startKernel = cpuCycles[1]

		counter += 1
		timeSpent = endTime - startTime
		#	if(percent < 100):
		#		break;	
		#results = perf.run()
		#percent = (int(results["total_replies"]) / int(results["total_connections"])) * 100
		#print results["total_connections"] , results["total_requests"], results["total_replies"], results["request_rate_per_sec"], "req/s", percent , "%", results["total_test_duration"], "seconds", counter 
		#print "CPU %=", cpuPercentage , "User = " , (cpuCycles[0] - startUser), ", System=,", (cpuCycles[1] - startKernel), "Memory % = ", process.memory_percent(), timeSpent	
		line = str((cpuPercentage , (cpuCycles[0] - startUser) , (cpuCycles[0] - lastUser) , (cpuCycles[1] - startKernel), (cpuCycles[1] - lastKernel), process.memory_percent(), timeSpent))
		line = line.strip("(")
		line = line.strip(")")
		target.write(line + "\n")

		lastUser = cpuCycles[0]
		lastKernel = cpuCycles[1]
		print line
		counter += 1
	target.close()
	########################### While
#### FOR
