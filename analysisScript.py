# -*- coding: utf-8 -*-
import os, os.path

#All files, INCREMENT 1-N for a given experiment.
lowestKernel = 0
lowestUser = 0
lowestCPU = 0
lowestFile = ""

highestKernel = 0
highestUser = 0
highestCPU = 0
highestFile = ""

avgCPU = 0
avgCpuAll12 = 0
totCon = 0
totRep = 0


#files_in_dir = os.listdir("./")
files_in_dir = ["1000_1", "1000_2", "1000_3", "1000_4", "1000_5"]

print files_in_dir
rateCPU = {}
rateAvgCPU = {}
rateMinCPU = {} 
rateMaksCPU = {}
avgCpuPerc = {}
avgMem = {}

for file in files_in_dir:
	totalCPU = 0
	if(file == "counter.py"):
		print "counter.py"
	else:
		rate = file.split("_")
		rate = int(rate[0].strip("_"))
		totalAmountOfRates = rate*10
		
		#Rate 1&      0.93&   0.88&   1.09&   0.31\%&         4,27\%  \\ \hline

		
		perc = 0
		avgCPU = 0
		data = open(file, "r")
		counter = 0
		for line in data:
			line = line.strip().split(",")
			#perc += float(line[0])
			counter += 1

			if("," in line[0]):
				line[0] = line[0].strip(",")
				
			perc += float(line[0])
			if(counter == 10):
				user = float(line[1].strip(" "))
				kernel = float(line[3].strip(" "))

				totalCPU = user + kernel 

				avgCPU += totalCPU
				
		print "-------------------------"
		print file
		#print "AvgCPU = ", avgCPU/12
		print "TotalCPU", totalCPU
		print "Cpu Percentage this run",  (perc/10)
		avgCpuAll12 += (perc/10)
		
		
		CPUperReq = (totalCPU)/totalAmountOfRates
		
		if(rate in rateCPU):
			rateCPU[rate] += CPUperReq
			rateAvgCPU[rate] += totalCPU
			
			if(totalCPU < rateMinCPU[rate]):
				rateMinCPU[rate] = totalCPU
			if(totalCPU > rateMaksCPU[rate]):
				rateMaksCPU[rate] = totalCPU
			
			avgCpuPerc[rate] += (perc/10)
			avgMem[rate] += float(line[5].strip(" "))
		else:
			rateCPU[rate] = CPUperReq
			rateAvgCPU[rate] = totalCPU #Total CPU for 300 seconds
			rateMinCPU[rate] = totalCPU
			rateMaksCPU[rate] = totalCPU
			avgCpuPerc[rate] = (perc/10)
			avgMem[rate] = float(line[5].strip(" "))
			
			


		print "totalAmountOfRates", totalAmountOfRates
		print "Price per request - idle CPU", CPUperReq
		print "-------------------------"
				#print [5] #Memory. Expected to be equal
print "-------------     SUMMARY    -------------"

#print "Aver", avgCpuAll12/12
print 
#sorted(numbers.items())

print "Ubuntu&    CPU/Con& Avg&  Min&    Max&    Avg. CPU\%&     Memory\% \\ \hline"
# Rate 1&    0.010& 0.93&   0.88&   1.09&   0.31\%&         4,27\%  \\ \hline
#Rate 5&     0.010& 0.93&   0.88&   1.09&   0.31\%&         4,27\%  \\ \hline
for key in sorted(rateCPU):
	
	print "Rate", key , "& " , format(rateCPU[key]/5, '.5f'), "& ", rateAvgCPU[key]/5 , "& ", rateMinCPU[key] , "& ", rateMaksCPU[key] , "& ", format(avgCpuPerc[key]/5, '.2f'), "& ", format(avgMem[key]/5, '.1f'),  "\\\ \hline"


