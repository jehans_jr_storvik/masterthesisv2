#! /bin/bash

brctl addbr br0
ip addr add 192.168.179.1/24 broadcast 192.168.179.255 dev br0
ip link set br0 up

ip tuntap add dev tap0 mode tap
ip link set tap0 up promisc on

brctl addif br0 tap0
dnsmasq --interface=br0 --bind-interfaces --dhcp-range=192.168.179.10,192.168.179.254

#qemu-system-x86_64 -enable-kvm -m 1024 -netdev tap,id=t0,ifname=tap0,script=no,downscript=no -device e1000,netdev=t0,id=nic0 <imagefile>
#add IP for api1-N on hostQemu
